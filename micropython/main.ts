//% color=#32b9b9 iconWidth=50 iconHeight=40
namespace TPBot {
    //% block="TPBot Init" blockType="command"
    export function TPBotInit(parameter: any, block: any){
        Generator.addImport("from mpython import *");
        Generator.addImport("import machine");		
        Generator.addImport("from machine import Pin");
        Generator.addImport("import time");		
        Generator.addImport("from tpbot import *");
        Generator.addCode(`tpbot = TPBOT(i2c)`); 
		
    }		
	
    //% block="Set left wheel speed at [lspeed]right wheel speed at [rspeed]" blockType="command"
    //% lspeed.shadow="range" lspeed.params.min=-100 lspeed.params.max=100 lspeed.defl=100
    //% rspeed.shadow="range" rspeed.params.min=-100 rspeed.params.max=100 rspeed.defl=100
    export function setWheels(parameter: any, block: any){
        let lspeed = parameter.lspeed.code;
        let rspeed = parameter.rspeed.code;
        Generator.addCode(`tpbot.set_wheel(${lspeed},${rspeed})`);
    }


    //% block="Go [direc] at speed [speed]%" blockType="command"
    //% speed.shadow="range" speed.params.min=0 speed.params.max=100 speed.defl=100
    //% direc.shadow="dropdown" direc.options="DriveDirection" direc.defl="DriveDirection.Forward"
    export function setTravelSpeed(parameter: any, block: any){
        let direc = parameter.direc.code;
        let speed = parameter.speed.code;
        Generator.addCode(`tpbot.set_travel_speed(${direc},${speed})`);
    }


    //% block="Go [direc] at speed [speed]% for [time] seconds" blockType="command"
    //% speed.shadow="range" speed.params.min=0 speed.params.max=100 speed.defl=100
    //% direc.shadow="dropdown" direc.options="DriveDirection" direc.defl="DriveDirection.Forward"
    //% time.shadow="number" time.defl="0"
    export function setTravelTime(parameter: any, block: any){
        let direc = parameter.direc.code;
        let speed = parameter.speed.code;
        let time = parameter.time.code;
        Generator.addCode(`tpbot.set_travel_speed(${direc},${speed})`);
		Generator.addCode(`time.sleep(${time})`);
		Generator.addCode(`tpbot.stop_car()`);
    }
	
	
    //% block="Stop the car immediately" blockType="command"
    export function stopCar(){
        Generator.addCode(`tpbot.stop_car()`);
    }
	

    //% block="Set headlight color to R:[r] G:[g] B:[b]" blockType="command"
    //% r.shadow="range" r.params.min=0 r.params.max=255 r.defl=83
    //% g.shadow="range" g.params.min=0 g.params.max=255 g.defl=202
    //% b.shadow="range" b.params.min=0 b.params.max=255 b.defl=236
    export function headlightRGB(parameter: any, block: any){
        let r = parameter.r.code;
        let g = parameter.g.code;
        let b = parameter.b.code;
        Generator.addCode(`tpbot.headlight_rgb(${r}, ${g},${b})`);
    }


    //% block="Set headlight color to[color]" blockType="command"
    //% color.shadow="colorPalette"
    export function headlightColor(parameter: any, block: any) {
        let color = parameter.color.code;
        Generator.addCode(`tpbot.headlight_color(${color})`);
    }


    //% block="Turn off the headlights" blockType="command"
    //% weight=20
    export function headlightClose(){
        Generator.addCode(`tpbot.headlight_close()`);
    }


    //% block="Sonar distance unit [unit]" blockType="reporter"
    //% unit.shadow="dropdown" unit.options="SonarUnit" unit.defl="SonarUnit.Centimeters"
    export function sonarReturn(parameter: any, block: any): number {
        let unit = parameter.unit.code;

        Generator.addCode(`tpbot.get_distance(${unit})`);
    }
	
	
    //% block="[side]line sensor detected [state]" blockType="boolean"
    //% side.shadow="dropdown" side.options="LineSide" side.defl="LineState.Left"
    //% state.shadow="dropdown" state.options="LineState" state.defl="LineSide.FindLine"
    export function trackSide(parameter: any, block: any) {
        let side = parameter.side.code;
        let state = parameter.state.code;        
        Generator.addCode(`tpbot.is_track_side(${side},${state})`);
    }
	
	
	    //% block="Set 180° servo [servo] angle to [angle] °" blockType="command"
    //% servo.shadow="dropdown" servo.options="ServoList" servo.defl="ServoList.S1"
    //% angle.shadow="angle" angle.params.edge=1
    export function setServo180(parameter: any, block: any){
        let servo = parameter.servo.code;
        let angle = parameter.angle.code;
        Generator.addCode(`tpbot.set_servo180(${servo},${angle})`);
    }


    //% block="Set 360° servo [servo] speed to [speed] %" blockType="command"
    //% servo.shadow="dropdown" servo.options="ServoList" servo.defl="ServoList.S1"
    //% speed.shadow="range" speed.params.min=-100 speed.params.max=100 speed.defl=100
    export function setServo360(parameter: any, block: any){
        let servo = parameter.servo.code;
        let speed = parameter.speed.code;
        Generator.addCode(`tpbot.set_servo360(${servo},${speed})`);
    }
	
	
}