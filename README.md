# 天蓬智能车TPBot


### 介绍



天蓬智能车TPBotMind+用户库


![输入图片说明](micropython/_images/featured.png)



### 积木




![输入图片说明](micropython/_images/%E7%A7%AF%E6%9C%A8.png)



#### 示例程序


![输入图片说明](micropython/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F.png)



 **巡线示例程序** 


![巡线](micropython/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F-%E5%B7%A1%E7%BA%BF.png)



 **超声波测距示例程序** 


![超声波](micropython/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F-%E8%B6%85%E5%A3%B0%E6%B3%A2.png)



### 许可证


MIT


### 支持列表




主板型号	实时模式	ArduinoC	MicroPython	备注
掌控板			√	
		



### 更新日志


 

- V0.0.1  2021-12-3 首次发布
-         2021-12-4 新增超声波测距积木、巡线积木